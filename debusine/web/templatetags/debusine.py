# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Template tag library to render Debusine UI elements."""

import logging
from datetime import datetime

from django import template
from django.conf import settings
from django.db.models import Model
from django.utils.html import format_html

from debusine.web.views.base import Widget
from debusine.web.views.ui_shortcuts import UIShortcut

logger = logging.getLogger("debusine.web")

register = template.Library()


@register.simple_tag
def ui_shortcuts(obj: Model) -> list[UIShortcut]:
    """Return the stored UI shortcuts for the given object."""
    stored = getattr(obj, "_ui_shortcuts", None)
    if stored is None:
        return []
    return stored


@register.simple_tag(takes_context=True)
def widget(context, widget: Widget) -> str:
    """Render a UI widget."""
    try:
        return widget.render(context)
    except Exception:
        if settings.DEBUG:
            raise
        # When running in production, avoid leaking possibly sensitive error
        # information while providing enough information for a potential bug
        # report to locate the stack trace in the logs
        logger.warning("widget %r failed to render", widget, exc_info=True)
        return format_html(
            "<span class='bg-danger text-white'>{ts} UTC: {widget}"
            " failed to render</span>",
            widget=widget.__class__.__name__,
            ts=datetime.utcnow().isoformat(),
        )
