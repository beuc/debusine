# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Views for debusine."""

from debusine.web.views.autopkgtest import AutopkgtestView  # noqa: F401
from debusine.web.views.lintian import LintianView  # noqa: F401
from debusine.web.views.views import (  # noqa: F401
    HomepageView,
    LogoutView,
    UserTokenCreateView,
    UserTokenDeleteView,
    UserTokenListView,
    UserTokenUpdateView,
)
