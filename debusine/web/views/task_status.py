# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Compute task status (workers, queue, etc.)."""
from django.views.generic import TemplateView

from debusine.db.models import WorkRequest


class TaskStatusView(TemplateView):
    """View to display task status."""

    template_name = "web/task_status.html"

    def get_context_data(self, *args, **kwargs):
        """Return context_data with task_queue."""
        context = super().get_context_data(**kwargs)

        context["task_queue"] = WorkRequest.objects.get_architecture_status()

        return context
