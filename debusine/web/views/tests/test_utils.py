# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Tests for the view tests utilities."""
from django.test import TestCase
from django.urls import reverse

import lxml
import lxml.objectify

from rest_framework import status

from debusine.web.views.tests.utils import ViewTestMixin


class TestUtils(ViewTestMixin, TestCase):
    """Tests for view test utility code."""

    def test_invalid_html(self):
        """Test warnings on invalid HTML."""
        response = self.client.get(reverse("homepage:homepage"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response.content = "<!DOCTYPE html><html><does-not-exist></html>"
        with self.assertRaisesRegex(
            AssertionError, r"1:HTML_UNKNOWN_TAG:Tag does-not-exist invalid"
        ):
            self.assertHTMLValid(response)

        response.content = "<!DOCTYPE html><html"
        with self.assertRaisesRegex(
            AssertionError,
            r"1:ERR_GT_REQUIRED:Couldn't find end of Start Tag html",
        ):
            self.assertHTMLValid(response)

    def test_workspace_list_not_found(self):
        """Test that workspace_list_table_rows errors when not found."""
        response = self.client.get(reverse("homepage:homepage"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        with self.assertRaisesRegex(
            AssertionError, r"page has no workspace list table"
        ):
            self.workspace_list_table_rows(tree)

    def test_collection_list_not_found(self):
        """Test that collection_list_table_rows errors when not found."""
        response = self.client.get(reverse("homepage:homepage"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        tree = self.assertHTMLValid(response)
        with self.assertRaisesRegex(
            AssertionError, r"page has no collection list table"
        ):
            self.collection_list_table_rows(tree)

    def test_assert_html_contents_equivalent(self):
        """Test that assertHTMLContentsEquivalent works as expected."""
        node = lxml.objectify.fromstring(
            '<span> some  na\n\n (<abbr title="Ext">E</abbr>)\n\n</span>\n     '
        )

        self.assertHTMLContentsEquivalent(
            node, '<span>some    na (<abbr title="Ext">E</abbr>)</span>'
        )

        with self.assertRaises(AssertionError):
            self.assertHTMLContentsEquivalent(node, "<span>Sample</span>")

        with self.assertRaises(AssertionError):
            self.assertHTMLContentsEquivalent(
                node, '<span>so me    na (<abbr title="Ext">E</abbr>)</span>'
            )
