# Generated by Django 3.2.19 on 2024-05-07 17:13

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0051_collection_workflow_data'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='collection',
            name='retains_artifacts_bool',
        ),
    ]
