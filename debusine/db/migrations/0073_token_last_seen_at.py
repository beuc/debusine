# Generated by Django 3.2.19 on 2024-08-19 14:55

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('db', '0072_worker_last_seen_at'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='worker',
            name='last_seen_at',
        ),
        migrations.AddField(
            model_name='token',
            name='last_seen_at',
            field=models.DateTimeField(
                blank=True,
                help_text='Last time that the token was used',
                null=True,
            ),
        ),
    ]
