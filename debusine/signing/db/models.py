# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Database models for the Debusine signing service."""

from pathlib import Path
from typing import BinaryIO, TYPE_CHECKING, TypeAlias

from django.conf import settings
from django.db import models

from debusine.signing.models import (
    BaseProtectedKey,
    ProtectedKey,
    ProtectedKeyNaCl,
)
from debusine.signing.openssl import openssl_generate, x509_fingerprint
from debusine.signing.sbsign import sbsign
from debusine.utils import calculate_hash

if TYPE_CHECKING:
    from django_stubs_ext.db.models import TypedModelMeta
else:
    TypedModelMeta = object


class _KeyPurpose(models.TextChoices):
    """Choices for Key.purpose."""

    UEFI = "uefi", "UEFI Secure Boot"
    OPENPGP = "openpgp", "OpenPGP"


class AuditLog(models.Model):
    """An audit log entry."""

    objects = models.Manager["AuditLog"]()

    class Event(models.TextChoices):
        GENERATE = "generate", "Generate"
        SIGN = "sign", "Sign"
        REGISTER = "register", "Register an externally-generated key"

    purpose = models.CharField(max_length=7, choices=_KeyPurpose.choices)
    fingerprint = models.CharField(max_length=64)
    event = models.CharField(max_length=8, choices=Event.choices)
    data = models.JSONField()
    created_at = models.DateTimeField(auto_now_add=True)
    created_by_work_request_id = models.IntegerField(blank=True, null=True)

    class Meta(TypedModelMeta):
        app_label = "signing"
        indexes = [
            models.Index(
                "purpose",
                "fingerprint",
                name="%(app_label)s_%(class)s_key_idx",
            ),
            models.Index(
                "created_by_work_request_id",
                name="%(app_label)s_%(class)s_wr_idx",
            ),
        ]


class KeyManager(models.Manager["Key"]):
    """Manager for the Key model."""

    def get_fingerprint(
        self,
        *,
        purpose: _KeyPurpose,
        public_key: bytes,
        log_file: BinaryIO | None = None,
    ) -> str:
        """Get the fingerprint for a public key."""
        match purpose:
            case _KeyPurpose.UEFI:
                return x509_fingerprint(public_key, log_file=log_file)
            case _KeyPurpose.OPENPGP:
                # Imported late because the gpg module has to be installed as a
                # .deb.
                from debusine.signing.gnupg import gpg_fingerprint

                return gpg_fingerprint(public_key)
            case _ as unreachable:
                raise AssertionError(f"Unexpected purpose: {unreachable}")

    def create_key(
        self,
        *,
        purpose: _KeyPurpose,
        fingerprint: str,
        private_key: BaseProtectedKey,
        public_key: bytes,
        description: str,
        event: AuditLog.Event = AuditLog.Event.GENERATE,
        created_by_work_request_id: int | None = None,
    ) -> "Key":
        """Create a new key and add an audit log entry for it."""
        key = self.create(
            purpose=purpose,
            fingerprint=fingerprint,
            private_key=private_key.dict(),
            public_key=public_key,
        )
        AuditLog.objects.create(
            event=event,
            purpose=purpose,
            fingerprint=fingerprint,
            data={"description": description},
            created_by_work_request_id=created_by_work_request_id,
        )
        return key

    def generate(
        self,
        purpose: _KeyPurpose,
        description: str,
        created_by_work_request_id: int,
        log_file: BinaryIO | None = None,
    ) -> "Key":
        """Generate a new key."""
        match purpose:
            case _KeyPurpose.UEFI:
                # For now we just hardcode the certificate lifetime: 15
                # years is enough to extend from the start of the
                # development period of a Debian release to the end of
                # Freexian's extended LTS period.
                days = 15 * 365
                private_key, public_key = openssl_generate(
                    description, days, log_file=log_file
                )
            case _KeyPurpose.OPENPGP:
                # Imported late because the gpg module has to be installed as a
                # .deb.
                from debusine.signing.gnupg import gpg_generate

                private_key, public_key = gpg_generate(description)
            case _ as unreachable:
                raise AssertionError(f"Unexpected purpose: {unreachable}")

        fingerprint = self.get_fingerprint(
            purpose=purpose, public_key=public_key, log_file=log_file
        )
        return self.create_key(
            purpose=purpose,
            fingerprint=fingerprint,
            private_key=ProtectedKeyNaCl.encrypt(
                settings.DEBUSINE_SIGNING_PRIVATE_KEYS[0].public_key,
                private_key,
            ),
            public_key=public_key,
            description=description,
            created_by_work_request_id=created_by_work_request_id,
        )


class Key(models.Model):
    """A key managed by the debusine signing service."""

    objects = KeyManager()

    Purpose: TypeAlias = _KeyPurpose

    purpose = models.CharField(max_length=7, choices=Purpose.choices)
    fingerprint = models.CharField(max_length=64)
    private_key = models.JSONField()
    public_key = models.BinaryField()
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(blank=True, null=True)

    class Meta(TypedModelMeta):
        app_label = "signing"
        constraints = [
            models.UniqueConstraint(
                fields=("purpose", "fingerprint"),
                name="%(app_label)s_%(class)s_unique_purpose_fingerprint",
            )
        ]

    @property
    def stored_private_key(self) -> ProtectedKey:
        """Access private_key as a pydantic model."""
        return ProtectedKey.parse_obj(self.private_key)

    def sign(
        self,
        data_path: Path,
        signature_path: Path,
        created_by_work_request_id: int,
        log_file: BinaryIO | None = None,
    ) -> None:
        """
        Sign data in `data_path` using this key.

        `signature_path` will be overwritten with the resulting signature.
        """
        match self.purpose:
            case _KeyPurpose.UEFI:
                sbsign(
                    self.stored_private_key,
                    self.public_key,
                    data_path=data_path,
                    signature_path=signature_path,
                    log_file=log_file,
                )
            case _KeyPurpose.OPENPGP:
                # Imported late because the gpg module has to be installed as a
                # .deb.
                from debusine.signing.gnupg import gpg_sign

                gpg_sign(
                    self.stored_private_key,
                    self.public_key,
                    data_path=data_path,
                    signature_path=signature_path,
                )
            case _ as unreachable:
                raise AssertionError(f"Unexpected purpose: {unreachable}")

        AuditLog.objects.create(
            event=AuditLog.Event.SIGN,
            purpose=self.purpose,
            fingerprint=self.fingerprint,
            data={"data_sha256": calculate_hash(data_path, "sha256").hex()},
            created_by_work_request_id=created_by_work_request_id,
        )
