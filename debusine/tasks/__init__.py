# Copyright 2022 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Debusine tasks.

Import this module using for example:
from debusine.tasks import RunCommandTask, TaskConfigError

Debusine tasks such as sbuild become available
"""

import sys
from typing import cast

from debusine.tasks._task import (
    BaseExternalTask,
    BaseTask,
    BaseTaskWithExecutor,
    RunCommandTask,
    TaskConfigError,
)

# Sub-tasks need to be imported in order to be available to BaseTask
# (e.g. for BaseTask.is_valid_task_name). They are registered via
# BaseTask.__init_subclass__.
from debusine.tasks.assemble_signed_source import (  # noqa: F401, I202
    AssembleSignedSource,
)
from debusine.tasks.autopkgtest import Autopkgtest  # noqa: F401, I202
from debusine.tasks.blhc import Blhc  # noqa: F401, I202
from debusine.tasks.lintian import Lintian  # noqa: F401, I202
from debusine.tasks.mmdebstrap import MmDebstrap  # noqa: F401, I202
from debusine.tasks.noop import Noop  # noqa: F401, I202
from debusine.tasks.piuparts import Piuparts  # noqa: F401
from debusine.tasks.sbuild import Sbuild  # noqa: F401, I202
from debusine.tasks.simplesystemimagebuild import (  # noqa: F401, I202
    SimpleSystemImageBuild,
)

# Import the documentation from where the code lives
__doc__ = cast(str, sys.modules[BaseTask.__module__].__doc__)

__all__ = [
    "BaseExternalTask",
    "BaseTask",
    "BaseTaskWithExecutor",
    "RunCommandTask",
    "TaskConfigError",
]
