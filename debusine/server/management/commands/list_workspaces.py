# Copyright 2023 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to list workspaces."""

from debusine.db.models import Workspace
from debusine.django.management.debusine_base_command import DebusineBaseCommand
from debusine.server.management.utils import print_workspaces


class Command(DebusineBaseCommand):
    """Command to list the workspaces."""

    help = "List all the workspaces"

    def handle(self, *args, **options):
        """List the workspaces."""
        workspaces = Workspace.objects.all()
        print_workspaces(self.stdout, workspaces)
