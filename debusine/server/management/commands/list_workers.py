# Copyright 2021 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""debusine-admin command to list workers."""

from debusine.db.models import Worker
from debusine.django.management.debusine_base_command import DebusineBaseCommand
from debusine.server.management.utils import print_workers


class Command(DebusineBaseCommand):
    """Command to list the workers."""

    help = "List all the workers"

    def handle(self, *args, **options):
        """List the workers."""
        workers = Worker.objects.all()
        print_workers(self.stdout, workers)
