# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""Models used by debusine server-side tasks."""

from typing import Any

try:
    import pydantic.v1 as pydantic
except ImportError:
    import pydantic as pydantic  # type: ignore

from debusine.tasks.models import BaseTaskData, LookupSingle


class ServerNoopData(BaseTaskData):
    """In-memory task data for the ServerNoop task."""

    exception: bool = False
    result: bool = False


class APTMirrorData(BaseTaskData):
    """In-memory task data for the APTMirror task."""

    collection: str
    url: pydantic.AnyUrl
    suite: str
    components: list[str] | None = None
    # TODO: This should ideally be optional, but discovery is harder than it
    # ought to be.  See https://bugs.debian.org/848194#49.
    architectures: list[str]
    signing_key: str | None = None

    @pydantic.root_validator
    @classmethod
    def check_suite_components_consistency(cls, values):
        """Components are only allowed/required for non-flat repositories."""
        if values["suite"].endswith("/"):
            if values.get("components") is not None:
                raise ValueError(
                    'Flat repositories (where suite ends with "/") must not '
                    'have components'
                )
        else:
            if values.get("components") is None:
                raise ValueError(
                    'Non-flat repositories (where suite does not end with '
                    '"/") must have components'
                )
        return values


class UpdateDerivedCollectionData(BaseTaskData):
    """In-memory task data for the UpdateDerivedCollection task."""

    base_collection: LookupSingle
    derived_collection: LookupSingle
    child_task_data: dict[str, Any] | None = None
    force: bool = False


class UpdateSuiteLintianCollectionData(UpdateDerivedCollectionData):
    """In-memory task data for the UpdateSuiteLintianCollection task."""
