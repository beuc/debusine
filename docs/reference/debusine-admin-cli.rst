.. _debusine-admin-cli:

==========================
The debusine-admin command
==========================

The ``debusine-admin`` command is the usual `django-admin
<https://docs.djangoproject.com/en/3.2/ref/django-admin/>`_ command
of a Django project. But debusine adds many `custom management commands
<https://docs.djangoproject.com/en/3.2/howto/custom-management-commands/>`_
that are documented on this page.

This command must be executed on the server while connected as the
``debusine-server`` user. For example, to run the command ``check``:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin check

For commands that are currently undocumented, you can still explore their
features by using the ``--help`` command line option:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin edit_worker_metadata --help

Command output
--------------

If a command is successful: nothing is printed and the return code is 0.

Managing workers
----------------

``manage_worker``
~~~~~~~~~~~~~~~~~

The command ``manage_worker`` has two actions: **enable** and **disable**.

Workers are **disabled** by default: they do not receive any tasks to run.

To enable a worker find its name or its token using the ``list_workers``
command and then enable the worker using the ``manage_worker``. To enable
the worker ``worker-01``:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_worker enable worker-01

To disable a worker: use ``disable`` instead of ``enable``. When disabling
a worker, any work requests assigned to the worker in RUNNING or PENDING status
will be de-assigned and assigned to another worker.

Disabling a worker only makes the worker's token disabled: the worker will not
be able to submit any results (the server will reject any communication
with HTTP 403) or interact with the server. No attempt is made to stop the
worker's current task.

To stop the running work requests on the worker run in the worker:

.. code-block:: console

  $ sudo systemctl stop debusine-worker

.. _debusine-admin-edit-worker-metadata:

``edit_worker_metadata``
~~~~~~~~~~~~~~~~~~~~~~~~

A debusine sysadmin can attach static metadata to a worker. To edit the
worker metadata for ``worker-01`` you would run:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin edit_worker_metadata worker-01

This launches ``sensible-editor`` on a file with the current metadata.
The worker metadata is presented (and expected to be formatted) in YAML.

.. note::

    The initial metadata is presented as an empty dictionary (``{}``).
    That line should be deleted when you start inputting your own
    metadata.

When exiting the editor, the worker metadata will be updated with whatever
got saved in the edited file (except if the YAML turns out to be invalid).

For non-interactive use, you can store the metadata in a file, and pass
that file with ``--set``:

.. code-block:: console

  $ cat > /tmp/metadata <<END
  system:architectures:
  - amd64
  - i386
  END
  $ sudo -u debusine-server debusine-admin edit_worker_metadata worker-01 --set /tmp/metadata
  debusine: metadata set for debusine-internal

``list_workers``
~~~~~~~~~~~~~~~~

List workers with information:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_workers
  Name                                     Registered                        Connected                         Token hash                                                        Enabled
  ---------------------------------------  --------------------------------  --------------------------------  ----------------------------------------------------------------  ---------
  debusine                                 2023-10-02T14:13:12.342630+00:00  2024-01-10T10:41:46.716838+00:00  34c9c6d7ae395323c2ec6050cf4513bbf1fa9947701a0d7ba402ceba2e702c2f  False
  ansible                                  2023-12-20T15:03:36.008399+00:00  -                                 0d5cbfe1539b189e2108ffefe7458b7651a6a688ca408d9568886c883693a7fc  True

  Number of workers: 2

If a worker is not connected at this time: the ``Connected`` column has ``-``.
If it's connected: it has the time that connected.

Workers can be enabled or disabled using the command ``manage_worker``.

Managing users
--------------

``create_user``
~~~~~~~~~~~~~~~

Users can be created using the ``create_user`` command. The password for the
new user will be printed.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_user john.doe john@example.org
  m;Ag[2BcyItI..=M

A user can login on the website and then create tokens to be used by debusine
client.

Tokens for a debusine user (to be used by debusine client) can also be created
using the command ``create_token``.

``manage_user``
~~~~~~~~~~~~~~~

Users can be managed using ``manage_user``. To change the email of the
``john.doe`` user to ``johnd@example.org``:

.. code-block:: console

  sudo -u debusine-server debusine-admin manage_user change-email john.doe johnd@example.org

``changepassword``
~~~~~~~~~~~~~~~~~~

Change the password for a user. The password is asked interactively:

.. code-block:: console

  sudo -u debusine-server debusine-admin changepassword john.doe

``create_super_user``
~~~~~~~~~~~~~~~~~~~~~

Create a super user. Super users have access to the `admin` section of
debusine-server (currently not used).

.. code-block:: console

  $ sudo -u debusine-server debusine-admin createsuperuser

The username is asked interactively.

``list_users``
~~~~~~~~~~~~~~

List the users with their `email` and `date_joined`.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_users

  username                  email              date_joined
  ------------------------  ------------------ ---------------------------------
  john.doe                  johnd@example.org   2024-01-10T14:10:52.667568+00:00

  Number of users: 1


Manage workspaces
-----------------

``create_workspace``
~~~~~~~~~~~~~~~~~~~~

Artifacts belong to workspaces. To create a workspace:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_workspace Debian

Workspaces are created ``Private`` by default (only registered users
can access its resources) and without a default expiration delay. Use
``manage_workspace`` to change the permissions or expiry delay.

``manage_workspace``
~~~~~~~~~~~~~~~~~~~~

Change a ``workspace``. The command has the options:

  * ``--private``: make the workspace private (only authenticated users can see the resources in it)
  * ``--public``: non-logged users can see the resources of the workspace
  * ``--default-expiration-delay [DAYS]``: set the default expiration delay for the artifacts that are created in this workspace.

To change the permissions of the workspace `Debian` to `public`:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_workspace Debian --public

``list_workspaces``
~~~~~~~~~~~~~~~~~~~

List the workspaces with information:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_workspaces
  Name    Public    Default Expiration Delay (days)    Default File Store      # Other File Stores
  ------  --------  ---------------------------------  --------------------  ---------------------
  System  True      Never                              Default (Local)                           0
  Debian  False     Never                              Default (Local)                           0

  Number of workspaces: 2

Manage work requests
--------------------

``list_work_requests``
~~~~~~~~~~~~~~~~~~~~~~

List the work requests and its status. Similar information can be displayed
using the web interface of debusine.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_work_requests
  id  worker                                   created_at                        started_at                        completed_at                      status     result
  ----  ---------------------------------------  --------------------------------  --------------------------------  --------------------------------  ---------  --------
  1  debusine                                 2023-10-02T14:17:54.213857+00:00  2023-10-02T14:17:54.291425+00:00  2023-10-02T14:18:58.489656+00:00  completed  success
  2  ansible-worker-2                         2023-10-06T10:04:06.731566+00:00  2023-10-06T10:04:06.806152+00:00  2023-10-06T10:05:07.050323+00:00  completed  success

  Number of work requests: 2

.. _debusine-admin-notification-channels:

Manage notification channels
----------------------------

``create_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Create a notification channel:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_notification_channel Debian-LTS email << EOF
  {
    "from": "admin@example.org",
    "to": ["lts@example.org"]
  }
  EOF

notification channels can be used when creating a work request.

Currently only the type ``email`` is implemented.

See :ref:`configure-notifications` for more information.

``manage_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

To change the name of a notification channel from ``Debian`` to ``Debian-LTS``:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_notification_channel change-name Debian Debian-LTS

To change the associated data to the notification channel:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin manage_notification_channel change-data Debian-LTS << EOF
  {
    "from": "admin@example.org",
    "to": ["new-to@example.org"]
  }
  EOF

``list_notification_channels``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

List the notification channels with their information.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_notification_channels

  name        method    data
  ----------  --------  -----------------------------------------------------------
  Debian-LTS  email     {'to': ['lts222@example.com'], 'from': 'admin@example.com'}

  Number of notification channels: 1

``delete_notification_channel``
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deletes a notification channel.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin delete_notification_channel Debian-LTS

Managing tokens
---------------

``create_token``
~~~~~~~~~~~~~~~~

Create a token. Must be associated to a user. The token is printed to the
stdout. Users can also create tokens using the web interface.

.. code-block:: console

  $ sudo -u debusine-server debusine-admin create_token john.doe
  ed73404d2edd232bc20955a2316a16c41e9b0bf2c240d6aceb7bf0706cb6d78f
  debian@debusine:~$

The tokens created by ``create_token`` are enabled by default.

In debusine, there can be tokens that are not associated to users. They are
created when a debusine-worker registers to debusine-server.

``list_tokens``
~~~~~~~~~~~~~~~

The command ``list_tokens`` lists all tokens by default. It is possible
to filter tokens by the owner or the token itself, using the options ``--owner``
or ``--token``, for example:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin list_tokens --owner OWNER
  $ sudo -u debusine-server debusine-admin list_tokens --token TOKEN

``delete_tokens``
~~~~~~~~~~~~~~~~~

Tokens can be removed using the ``delete_tokens`` command. By default, it asks
for interactive confirmation unless ``--yes`` is used. See the options using:

.. code-block:: console

  $ sudo -u debusine-server debusine-admin delete_tokens --help

Administrative commands
-----------------------

.. _command-delete-expired:

``delete_expired``
~~~~~~~~~~~~~~~~~~

Delete expired resources

.. code-block:: console

  $ sudo -u debusine-server debusine-admin delete_expired

The ``debusine-server`` package installs a systemd timer to run this
command daily.

.. _command-monthly-cleanup:

``monthly_cleanup``
~~~~~~~~~~~~~~~~~~~

Report and fix inconsistencies in Debusine's DB and storage.

.. code-block::

  $ sudo -u debusine-server debusine-admin monthly_cleanup

The ``debusine-server`` package installs a systemd timer to run this
command monthly.
