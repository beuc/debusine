========================
Dependency-wait handling
========================

.. _task-delay:

Delay task
==========

This :ref:`wait task <explanation-tasks>` completes after its
``delay_until`` timestamp.

The ``task_data`` for this task may contain the following keys:

* ``delay_until`` (datetime, required): a timestamp on or after which this
  task may be run

As with other wait tasks, the scheduler marks it running (without assigning
a worker) as soon as possible after it is pending, after which it is
considered to be waiting for its event to happen.  Separately, it handles
the completion of this particular task directly, by marking it as
successfully completed if the current time is greater than or equal to its
``delay_until`` timestamp.

.. _action-retry-with-delays:

``retry-with-delays``
=====================

This action is used in ``on_failure`` event reactions.  It causes the work
request to be retried automatically with various parameters, adding a
dependency on a newly-created :ref:`task-delay`.

The current delay scheme is limited and simplistic, but we expect that more
complex schemes can be added as variations on the parameters to this action.

* ``delays`` (list, required): a list of delays to apply to each successive
  retry; each item is an integer suffixed with ``m`` for minutes, ``h`` for
  hours, ``d`` for days, or ``w`` for weeks.

The workflow data model for work requests gains a ``retry_count`` field,
defaulting to 0 and incrementing on each successive retry.  When this action
runs, it creates a :ref:`task-delay` with its ``delay_until`` field set to
the current time plus the item from ``delays`` corresponding to the current
retry count, adds a dependency from its work request to that, and marks its
work request as blocked on that dependency.  If the retry count is greater
than the number of items in ``delays``, then the action does nothing.

Sbuild workflow changes
=======================

The :ref:`sbuild workflow <workflow-sbuild>` gains a ``retry_delays`` field
in its task data, defined as in the :ref:`action-retry-with-delays` action.
If set, it adds a corresponding ``on_failure``
:ref:`action-retry-with-delays` action to each of the ``sbuild`` work
requests it creates.
