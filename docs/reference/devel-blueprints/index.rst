.. _development-blueprints:

======================
Development blueprints
======================

.. toctree::

    debootstrap-task
    acl
    workflows
    workflow-coordination
    ui-views
    package-upload
    dep-wait
