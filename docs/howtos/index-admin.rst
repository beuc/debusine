========================
Tasks for administrators
========================

.. toctree::

   set-up-debusine-server
   set-up-debusine-worker
   set-up-debusine-signing
   set-up-incus
   add-new-worker
   add-new-user
   configure-manage-worker
   enable-logins-with-gitlab
   enable-notifications
   set-up-apt-mirroring
