#!/bin/sh

set -e

# shellcheck disable=SC2034
case "$1" in
	configure)
		user="debusine-signing"
		group="$user"

		# Create debusine-signing user
		if ! getent passwd "$user" > /dev/null 2>&1
		then
			useradd --system \
				--comment "Debusine Signing System user" \
				--home-dir "/var/lib/debusine/signing" \
				--create-home \
				"$user"
		fi
    
		# Create directories
		directories="/etc/debusine/signing /var/log/debusine/signing"

		# shellcheck disable=SC2086
		mkdir -p $directories

		# Change directory permissions
		# shellcheck disable=SC2086
		chown "$user:$group" $directories

		# shellcheck source=/dev/null
		. /usr/share/debconf/confmodule
		# shellcheck source=/dev/null
		. /usr/share/dbconfig-common/dpkg/postinst.pgsql

		if [ -e /etc/debusine/signing/db_postgresql.py ]; then
			chown "$user:$group" /etc/debusine/signing/db_postgresql.py
			chmod 600 /etc/debusine/signing/db_postgresql.py
		fi

		dbc_first_version="0.4.2~1"
		dbc_generate_include="template:/etc/debusine/signing/db_postgresql.py"
		dbc_generate_include_args="-U -o template_infile=/etc/debusine/signing/db_postgresql.py.template"
		dbc_generate_include_owner="$user:$group"
		dbc_generate_include_perms=600
		dbc_go debusine-signing "$@"

		# workaround dbconfig replacing the empty string with localhost (#429841)
		if ! dbc_no_thanks ; then
			db_get debusine-signing/remote/host && host="$RET"
			db_get debusine-signing/pgsql/method
			if [ "$RET" = "Unix socket" ] ; then
				host=""
			fi
			sed -i "s/_DBC_HOST_/$host/" /etc/debusine/signing/db_postgresql.py
		fi
	;;
esac

#DEBHELPER#

exit 0
