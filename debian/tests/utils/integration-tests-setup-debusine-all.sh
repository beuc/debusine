#!/bin/sh

set -e

# Set up the debusine server, worker, and client.

debian/tests/utils/integration-tests-setup-debusine-server.sh
worker_token=$(debian/tests/utils/integration-tests-setup-debusine-worker.sh)

sudo -u debusine-server debusine-admin manage_worker enable "$worker_token"

# Some integration might try to download an artifact (for example, for debugging
# purposes of displaying a task log file). To keep the implementation easy:
# make the workspace public
sudo -u debusine-server debusine-admin manage_workspace System --public

debian/tests/utils/integration-tests-setup-debusine-client.sh
