#!/usr/bin/env python3

# Copyright 2024 The Debusine Developers
# See the AUTHORS file at the top-level directory of this distribution
#
# This file is part of Debusine. It is subject to the license terms
# in the LICENSE file found in the top-level directory of this
# distribution. No part of Debusine, including this file, may be copied,
# modified, propagated, or distributed except according to the terms
# contained in the LICENSE file.

"""
Debusine integration tests.

Test signing service.
"""

import base64
import json
import subprocess
from pathlib import Path
from unittest import TestCase

import requests

from utils.client import Client
from utils.common import Configuration, launch_tests
from utils.integration_test_helpers_mixin import IntegrationTestHelpersMixin
from utils.server import DebusineServer

import yaml

from debusine.artifacts.models import ArtifactCategory
from debusine.client.models import ArtifactResponse, FileResponse


class IntegrationSigningTests(IntegrationTestHelpersMixin, TestCase):
    """
    Integration tests for the signing service.

    These tests assume:
    - debusine-server is running
    - debusine-signing is running (connected to the server)
    - debusine-client is correctly configured
    """

    def setUp(self) -> None:
        """Initialize test."""
        # If debusine-server or nginx was launched just before this test,
        # then debusine-server might not be available yet.  Wait for
        # debusine-server to be reachable if it's not ready.
        self.assertTrue(
            DebusineServer.wait_for_server_ready(),
            f"debusine-server should be available (in "
            f"{Configuration.get_base_url()}) before the integration tests "
            f"are run",
        )

    @staticmethod
    def download_and_extract_binary(package_name: str, target: Path) -> Path:
        """Download and extract a binary package."""
        subprocess.check_call(["apt-get", "download", package_name], cwd=target)
        subprocess.check_call(
            [
                "dpkg-deb",
                "-x",
                next(target.glob(f"{package_name}_*.deb")),
                package_name,
            ],
            cwd=target,
        )
        return target / package_name

    @classmethod
    def verify_uefi_signature(
        cls,
        signing_key_artifact: ArtifactResponse,
        signed_file: FileResponse,
    ):
        """Verify a signed file against its UEFI signing key."""
        with cls._temporary_directory() as temp_path:
            (certificate := temp_path / "uefi.crt").write_bytes(
                base64.b64decode(
                    signing_key_artifact["data"]["public_key"].encode()
                )
            )
            (signature := temp_path / "image.sig").write_bytes(
                requests.get(signed_file["url"]).content
            )
            subprocess.check_call(
                ["sbverify", "--cert", certificate, signature], cwd=temp_path
            )

    def test_generate_and_sign(self) -> None:
        """Generate a key and sign something with it."""
        result = DebusineServer.execute_command(
            "create_work_request",
            "signing",
            "generatekey",
            stdin=yaml.safe_dump(
                {"purpose": "uefi", "description": "Test key"}
            ),
        )
        self.assertEqual(result.returncode, 0)
        work_request_id = yaml.safe_load(result.stdout)["work_request_id"]

        # The worker should get the new work request and start executing it
        status = Client.wait_for_work_request_completed(
            work_request_id, "success"
        )
        if not status:
            self.print_work_request_debug_logs(work_request_id)
        self.assertTrue(status)

        work_request = Client.execute_command(
            "show-work-request", work_request_id
        )

        [signing_key_artifact] = [
            artifact
            for artifact in work_request["artifacts"]
            if artifact["category"] == ArtifactCategory.SIGNING_KEY
        ]
        self.assertEqual(signing_key_artifact["data"]["purpose"], "uefi")
        self.assertIn("fingerprint", signing_key_artifact["data"])
        self.assertIn("public_key", signing_key_artifact["data"])

        # Sign something real, if we know how to find it.
        if (
            subprocess.check_output(
                ["dpkg", "--print-architecture"], text=True
            ).strip()
            == "amd64"
        ):
            template_package_name = "grub-efi-amd64-signed-template"
            with self._temporary_directory() as temp_path:
                extracted_template = self.download_and_extract_binary(
                    template_package_name, temp_path
                )
                packages = json.loads(
                    (
                        extracted_template
                        / "usr/share/code-signing"
                        / template_package_name
                        / "files.json"
                    ).read_text()
                )["packages"]

                signing_input_files: dict[str, bytes] = {}
                for package_name, metadata in packages.items():
                    extracted_package = self.download_and_extract_binary(
                        package_name, temp_path
                    )
                    for file in metadata["files"]:
                        self.assertEqual(file["sig_type"], "efi")
                        self.assertFalse(file["file"].startswith("/"))
                        signing_input_files[
                            f"{package_name}/{file['file']}"
                        ] = (extracted_package / file["file"]).read_bytes()

                signing_input_id = self.create_artifact_signing_input(
                    signing_input_files
                )

            result = DebusineServer.execute_command(
                "create_work_request",
                "signing",
                "sign",
                stdin=yaml.safe_dump(
                    {
                        "purpose": "uefi",
                        "unsigned": signing_input_id,
                        "key": signing_key_artifact["id"],
                    }
                ),
            )
            self.assertEqual(result.returncode, 0)
            work_request_id = yaml.safe_load(result.stdout)["work_request_id"]

            status = Client.wait_for_work_request_completed(
                work_request_id, "success"
            )
            if not status:
                self.print_work_request_debug_logs(work_request_id)
            self.assertTrue(status)

            work_request = Client.execute_command(
                "show-work-request", work_request_id
            )

            # The requested files were signed.
            [signing_output_artifact] = [
                artifact
                for artifact in work_request["artifacts"]
                if artifact["category"] == ArtifactCategory.SIGNING_OUTPUT
            ]
            self.assertEqual(signing_output_artifact["data"]["purpose"], "uefi")
            self.assertEqual(
                signing_output_artifact["data"]["fingerprint"],
                signing_key_artifact["data"]["fingerprint"],
            )
            self.assertCountEqual(
                signing_output_artifact["data"]["results"],
                [
                    {
                        "file": name,
                        "output_file": f"{name}.sig",
                        "error_message": None,
                    }
                    for name in signing_input_files
                ],
            )
            self.assertCountEqual(
                list(signing_output_artifact["files"]),
                [f"{name}.sig" for name in signing_input_files],
            )
            for name in signing_input_files:
                self.verify_uefi_signature(
                    signing_key_artifact,
                    signing_output_artifact["files"][f"{name}.sig"],
                )

            # Assemble a signed source package.  We need an environment for
            # this.
            self.create_system_images_mmdebstrap(["bookworm"])
            template_artifact_id = self.create_artifact_binary(
                "grub-efi-amd64-signed-template"
            )

            work_request_id = Client.execute_command(
                "create-work-request",
                "assemblesignedsource",
                stdin=yaml.safe_dump(
                    {
                        "environment": "debian/match:codename=bookworm",
                        "template": template_artifact_id,
                        "signed": [signing_output_artifact["id"]],
                    }
                ),
            )["work_request_id"]

            status = Client.wait_for_work_request_completed(
                work_request_id, "success"
            )
            if not status:
                self.print_work_request_debug_logs(work_request_id)
            self.assertTrue(status)

            work_request = Client.execute_command(
                "show-work-request", work_request_id
            )

            # The resulting source package contains all the signed files.
            [source_package_artifact] = [
                artifact
                for artifact in work_request["artifacts"]
                if artifact["category"] == ArtifactCategory.SOURCE_PACKAGE
            ]
            self.assertEqual(
                source_package_artifact["data"]["name"], "grub-efi-amd64-signed"
            )
            with self._temporary_directory() as temp_path:
                Client.execute_command(
                    "download-artifact",
                    "--target-directory",
                    temp_path,
                    source_package_artifact["id"],
                )
                subprocess.check_call(
                    [
                        "dpkg-source",
                        "-x",
                        next(temp_path.glob("*.dsc")).name,
                        "unpacked",
                    ],
                    cwd=temp_path,
                )
                for name in signing_input_files:
                    self.assertTrue(
                        (
                            temp_path
                            / "unpacked"
                            / "debian"
                            / "signatures"
                            / f"{name}.sig"
                        ).exists(),
                        f"Assembled source package lacks signature of {name}",
                    )


if __name__ == "__main__":
    launch_tests("Signing task integration tests for debusine")
